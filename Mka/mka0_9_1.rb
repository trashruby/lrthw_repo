
#loads strings for choices at the beginning of the program
#Requires module Puzzle
Dir["./txt/*.rb"].each {|file| load file }


class Room
  def enter()
    Dir["./mod/*.rb"].each {|file| require file }
    puts "for subclasses"
    exit(1)
  end
end


class Engine
  
  def initialize(room_map)
    @room_map = room_map
  end
  
  def play()
    current_room = @room_map.opening_room()
    last_room = @room_map.next_room('fishing')
    
    while current_room != last_room
      next_room_name = current_room.enter()
      current_room = @room_map.next_room(next_room_name);
    end
    
    
    current_room.enter()
  end
end

class Men_brk < Room
  
  @@mtl = [
    "You've given up all hope. The void slowly crushes you.",
     "You've simply lost all ability to function. Time to sleep.",
     "The feelings of guilt consume you. You don't know if you're cut out for this life.",
     "You slowly step back and start walking home. A single tear drops down your cheeck as you contemplate yourself.",
     "Pausing breifly you stop and think. Why did you do this... why this.... time to smoke a cig and take a nap."
  ]
  
  def enter()
    puts @@mtl[rand(0..(@@mtl.length - 1))]
    exit(1)
  end
end

class House < Room

  
  
  def enter()
    slp
    ihouse
    print "> "
    
    action = $stdin.gets.chomp.downcase
    
    if action == "cig"
      cigh
      @cigcount = 1
      return 'garage'
      
    elsif action == "fight"
      fighth
      return 'house'
      
    elsif action == "weed"
      weedh
      return 'house'
      
    elsif action == "poop"
      pooph
      return 'house'
      
    elsif action == "go outside"
      
      if @cigcount == 1
        goh
        return 'outside'
        
      else
        puts "You can't head out without smoking your first cancer stick of the day"
        return 'house'
      end
      
    elsif action == "think"
      thinkh
      return 'house'
      
    elsif action == "talk"
      talkh
      return 'house'
      
    else
      insults
      return 'house'
      
    end 
  end
end

class Outside < Room

  
  
  def enter()
    
    
    ioutside()
    print "> "
    
    action = $stdin.gets.chomp.downcase
    
    if action == "cig"
      @cigcount = 1
      cigo
      return 'outside'
      
    elsif action == "fight"
      fighto
      return 'outside'
      
    elsif action == "weed"
      weedo
      return 'outside'
      
    elsif action == "poop"
      poopo
      return 'outside'
      
    elsif action == "go to the garage"
      if @cigcount == 1
        goo
        return 'garage'
      
      else
        puts "You are pretty confused, you should probably smoke a stoge!"
        return 'garage'
      end
      
    elsif action == "think"
      thinko
      return 'outside'
      
    elsif action == "talk"
      talko
      return 'outside'
      
    else
      insults
      return 'outside'
    end
  
  end
end

class Garage < Room
  
  def enter()
    
    igarage
    
    def lock()
      @lock = Puzz.comb_room

    end
    lock(time) == 9
    lock(prompt) == "[Dial]> "
    lock(dialog_1) == gtxt_1
    lock(dialog_2) == gtxt_2
    lock(dialog_3) == gtxt_3
    lock(pass) == passg
    lock(fail) == failg
    lock(room_1) == 'chrismar'
    lock(room_2) == 'men_brkd'
    lock
  end
end

class Chrismar < Room

  
  def enter()
    
    ichrismar
    print "> "
    
    action = $stdin.gets.chomp.downcase
    
    if action == "cig"
      cigc
      @cigcount = 1
      return 'chrismar'
      
    elsif action == "fight"
      fightc
      return 'chrismar'
      
    elsif action == "weed"
      weedc
      return 'chrismar'
      
    elsif action == "poop"
      poopc
      return 'chrismar'
      
    elsif action == "go elsewhere"
      goc
      return 'men_brkd'
      
    elsif action == "think"
      talkc
      return 'chrismar'
    elsif action == "talk"
      
      if @cigcount == 1
        talkc
        return 'chris_fight'
        
      else
        puts "You don't feel like talking at the moment."
        sleep 5
        return 'chrismar'
      end
      
    else
      insults
      return 'chrismar'
    end
  
  end
end

class Chrisfight < Room
  
  
  def enter()
  
    chris_fighti
    print "> "
    action = $stdin.gets.chomp.downcase
  
    if action == "fight"
      
      fight_cf
      chris_fight = Puzz.new()
      chris_fight.time = 7
      chris_fight.prompt = "[Ruckus]> "
      chris_fight.dialog_1 = cf_txt_1
      chris_fight.dialog_2 = cf_txt_2
      chris_fight.dialog_3 = cf_txt_3
      chris_fight.pass = pass_cf
      chris_fight.fail = fail_cf
      chris_fight.room_1 = 'conv'
      chris_fight.room_2 = 'men_brkd'
      chris_fight.comb_room()
    elsif action == "think"
      think_cf
      sleep 3
      return 'chris_fight'
    else
      puts "What the fuck are you doing, you should really"
      slp
      puts "[think] about the situation you're in right now."
      slp
      return 'chris_fight'
    end
  end
end

class Conve < Room
  
  
  def enter()
    
    iconve
    print "> "
    
    action = $stdin.gets.chomp.downcase
    
    if action == "cig"
      cig_conv
      @cigcount = 1
      return 'conv'
      
    elsif action == "fight"
      fight_conv
      return 'conv'
      
    elsif action == "weed"
      weed_conv
      return 'conv'
      
    elsif action == "poop"
      poop_conv
      return 'conv'
      
    elsif action == "go elsewhere"
      go_conv
      return 'conv'
      
    elsif action == "think"
      think_conv
      return 'conv'
      
    elsif action == "talk"
      
      if @cigcount == 1
        talk_conv
        return 'conv_fight'
        
      else
        puts "You can't handle this at the moment, the stress is too much."
        return 'men_brkd'
      end
    
    else
      insults
      return 'conv'
    end
  
  end
end

class Convefight < Room
  
  def enter()
  
    iconvfight
    print "> "
    
    action = $stdin.gets.chomp.downcase
    
    if action == "fight"
      
      fight_conv
      conv_fight = Puzz.new()
      conv_fight.time = 5
      conv_fight.prompt = "[Beatdown]> "
      conv_fight.dialog_1 =
      conv_fight.dialog_2 =
      conv_fight.dialog_3 =
      conv_fight.pass = pass_conv
      conv_fight.fail = fail_conv
      conv_fight.room_1 = 'fishing'
      conv_fight.room_2 = 'men_brkd'
      conv_fight.comb_room()
    
    elsif action == "think"
      think_conv
      return 'conv_fight'
    
    else
      puts "You need to [think] about the situation you are in right now."
      sleep 2
      return 'conv_fight'
    end
  
  end
end

class Fishing < Room
  
  
  def enter()
    
    ifishing
    slp
    puts "CONGLATURATIONS! YOU JUST FISHED!"
    slp
    puts "YOU HAVE LIVED A DAY IN MILES SHOES!"
    slp
    puts "THANK YOU FOR PLAYING GAME<!"
    sleep 6
  end
end

class Map
  @@rooms = {
    'house' => House.new(),
    'outside' => Outside.new(),
    'garage' => Garage.new(),
    'chrismar' => Chrismar.new(),
    'chris_fight' => Chrisfight.new(),
    'conv' => Conve.new(),
    'conv_fight' => Convefight.new(),
    'fishing' => Fishing.new(),
    'men_brkd' => Men_brk.new(),
  }
  
  def initialize(start_room)
    @start_room = start_room
  end
  
  def next_room(room_name)
    val = @@rooms[room_name]
    return val
  end
  
  def opening_room()
    return next_room(@start_room)
  end
end

a_map = Map.new('house')
a_game = Engine.new(a_map)
puts "At any time when prompted by '>' you may type 'think' to check what actions are available."
a_game.play()
